import React, { Component } from 'react';
import Header from '../Header';

class Chat extends Component {
  render() {
    return (
      <div>
        <Header />
        <h1>This is a chat page</h1>
      </div>
    );
  }
}

export default Chat;