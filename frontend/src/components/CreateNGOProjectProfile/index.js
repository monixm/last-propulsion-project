import React, { Component } from 'react';
import { connect } from 'react-redux';
import './index.css';
import DescriptionPopup from './descriptionPopup';
import FocusPopup from './focusPopup';
import ContactPopup from './contactPopup';
import ProfilePopup from "./profilePopup";
import PrivacyPopup from "./privacyPopup";
import location_logo from '../../assets/location_logo.svg';
import upload_image from '../../assets/upload-image.svg';
import { createNGOProjectProfileAction } from '../../store/actions/CreateNGOProjectProfileAction';

class Feed extends Component {
    constructor(props){
        super(props);
        this.state = {
            information: {
                email:'',
                code:'',
                password:'',
                passwordConfirm:'',
                organisationName: '',
                organisationType:'',
                organisationLocation: '',
                organisationDescription: '',
                organisationTerms:'',
                privacy_setting:'',
                organisationDocument:'',
                profilePicture:'',
              organisationFocus: {
                  social:'',
                  languages:'',
                  sports:'',
                  artsCulture:'',
                  coaching:'',
                  food:'',
                  politics:'',
                  items:''
              },
                organisationContact: {
                    website:'',
                    phone:''
                },
                organisationProfile: {
                  facebook:'',
                    instagram:'',
                    linkedIn:''
                },
            },
          showDescriptionPopup: false,
          showFocusPopup: false,
          showContactPopup: false,
            showProfilePopup: false,
            showPrivacyPopup: false,
        }
    }

    toggleDescriptionPopup() {
    this.setState({
        showDescriptionPopup: !this.state.showDescriptionPopup
    });
    }

    toggleFocusPopup() {
        this.setState({
            showFocusPopup: !this.state.showFocusPopup
        });
    }

    toggleContactPopup() {
        this.setState({
            showContactPopup: !this.state.showContactPopup
        });
    }

    toggleProfilePopup() {
        this.setState({
            showProfilePopup: !this.state.showProfilePopup
        });
    }

     togglePrivacyPopup() {
        this.setState({
            showPrivacyPopup: !this.state.showPrivacyPopup
        });
    }

    changeSocialValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.social = e.currentTarget.value
        this.setState(newState)
    }

    changeLanguagesValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.languages = e.currentTarget.value
        this.setState(newState)
    }

    changeSportsValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.sports = e.currentTarget.value
        this.setState(newState)
    }

    changeArtsCultureValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.artsCulture = e.currentTarget.value
        this.setState(newState)
    }

    changeCoachingValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.coaching = e.currentTarget.value
        this.setState(newState)
    }

    changeFoodValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.food = e.currentTarget.value
        this.setState(newState)
    }

    changePoliticsValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.politics = e.currentTarget.value
        this.setState(newState)
    }

    changeDescriptionValue = e => {
        let newState = {...this.state}
        newState.information.organisationDescription = e.currentTarget.value
        this.setState(newState)
    }

    changeItemsValue = e => {
        let newState = {...this.state}
        newState.information.organisationFocus.items = e.currentTarget.value
        this.setState(newState)
    }

    changeWebsiteValue = e => {
        let newState = {...this.state}
        newState.information.organisationContact.website = e.currentTarget.value
        this.setState(newState)
    }

    changePhoneValue = e => {
        let newState = {...this.state}
        newState.information.organisationContact.phone = e.currentTarget.value
        this.setState(newState)
    }

    changeFacebookValue = e => {
        let newState = {...this.state}
        newState.information.organisationProfile.facebook = e.currentTarget.value
        this.setState(newState)
    }

    changeInstagramValue = e => {
        let newState = {...this.state}
        newState.information.organisationProfile.instagram = e.currentTarget.value
        this.setState(newState)
    }

    changeLinkedInValue = e => {
        let newState = {...this.state}
        newState.information.organisationProfile.linkedIn = e.currentTarget.value
        this.setState(newState)
    }

    handleTypeInput = e => {
        let newState = {...this.state}
        newState.information.organisationType = e.currentTarget.value
        this.setState(newState)
    }

    handleTermsInput = e => {
        let newState = {...this.state}
        newState.information.organisationTerms = e.currentTarget.value
        this.setState(newState)
    }

    handlePrivacyInput = e => {
        let newState = {...this.state}
        newState.information.privacy_setting = e.currentTarget.value
        this.setState(newState)
    }

    handleCreateProfile = () => {
        console.log(this.state)
        this.props.dispatch(createNGOProjectProfileAction(this.state.information))
    }

    documentHandler = e => {
        let newState = {...this.state}
        newState.information.organisationDocument = e.target.files[0]
        this.setState(newState)
    }

    profilePictureHandler = e => {
        let newState = {...this.state}
        newState.information.profilePicture = e.target.files[0]
        this.setState(newState)
    }

    nameValue = e => {
        let newState = {...this.state}
        newState.information.organisationName = e.currentTarget.value
        this.setState(newState)
    }

    locationValue = e => {
        let newState = {...this.state}
        newState.information.organisationLocation = e.currentTarget.value
        this.setState(newState)
    }

    emailValue = e => {
        let newState = {...this.state}
        newState.information.email = e.currentTarget.value
        this.setState(newState)
    }

    codeValue = e => {
        let newState = {...this.state}
        newState.information.code = e.currentTarget.value
        this.setState(newState)
    }

    passwordValue = e => {
        let newState = {...this.state}
        newState.information.password = e.currentTarget.value
        this.setState(newState)
    }

    confirmValue = e => {
        let newState = {...this.state}
        newState.information.passwordConfirm = e.currentTarget.value
        this.setState(newState)
    }



    render() {

        return (
            <div>
                <p className='ngo-choose'>Please choose what fits you best:</p>
            <div>
                <input className='ngo-radio' value='Non-profit organisation' type="radio" id="non-profit" name="radioA"
                   onClick={this.handleTypeInput} />
                   <label className='ngo-radio-label' htmlFor="non-profit">Non-profit <br />organisation</label>
                <input className='project-radio' value='Project' type="radio" id="project" name="radioA"
                   onClick={this.handleTypeInput} />
                   <label className='project-radio-label' htmlFor="project">Project</label>
            </div>
                <p className="email-org">Email</p>
                    <input className='email-org-input' id='email-org' value={this.state.information.email}
                           onChange={this.emailValue} type='text' name='email' required/>
                 <p className="code-org">Code</p>
                    <input className='code-org-input' id='code-org' value={this.state.information.code}
                           onChange={this.codeValue} type='text' name='code' required/>
                 <p className="password-org">Password</p>
                    <input className='password-org-input' id='password-org' value={this.state.password}
                           onChange={this.passwordValue} type='text' name='password' required/>
                 <p className="confirm-org">Confirm your password</p>
                    <input className='confirm-org-input' id='confirm-org' value={this.state.passwordConfirm}
                           onChange={this.confirmValue} type='text' name='code' required/>
                <form className='name-org-form'>
                    <p className="name-org">Name</p>
                    <input className='name-org-input' id='name-org' value={this.state.information.organisationName}
                           onChange={this.nameValue} type='text' name='name' required/>
                </form>
                <form className='location-org-form'>
                    <p className="location-org">Where are you located?</p><img className='location-logo' src={location_logo} alt=''/>
                    <input className='location-org-input' id='location-org' value={this.state.information.organisationLocation}
                           onChange={this.locationValue} type='text' name='location' required/>
                </form>
                <div>
                    <p className="ngo-pro-desc">Description</p>
                    <p className="ngo-pro-upload"><b>Please upload a verification document:</b>
                        <div className='doc-small-font'>(this is to secure the safety of individuals who will collaborate with you)</div></p>
                    <button onClick={() => this.documentInput.click()} className='btn-ngo-pro-upload'>Upload document</button>
                    <input style={{display: 'none'}}
                           type='file'
                           onChange={this.documentHandler}
                           ref={fileInput => this.documentInput = fileInput}/>
                    <button className='btn-fill-out' onClick={this.toggleDescriptionPopup.bind(this)}>Fill out</button>
                    <p className="ngo-pro-focus"><b>What kind of focus does your organisation or project have? </b><br />
                        <div className='doc-small-font'>(please add keywords)</div></p>
                    <button className='btn-fill-out-focus' onClick={this.toggleFocusPopup.bind(this)}>Fill out</button>
                    <p className="org-contact-info">Contact information</p>
                    <button className='btn-org-contact-info' onClick={this.toggleContactPopup.bind(this)}>Fill out</button>
                    <p className='ngo-pro-connect'>Would you like to connect to Facebook page, Instagram, Linkedin?</p>
                    <button className='btn-org-profile-1' onClick={this.toggleProfilePopup.bind(this)}>Add profile</button>
                    <p className='ngo-pro-privacy' >Privacy settings</p>
                    <button className='btn-org-privacy' onClick={this.togglePrivacyPopup.bind(this)}>Manage</button>
                    <p className='ngo-pro-picture'>Please add a profile picture:</p>
                    <img onClick={() => this.fileInput.click()} className='image_upload' src={upload_image} alt=''/>
                    <input style={{display: 'none'}}
                           type='file'
                           onChange={this.profilePictureHandler}
                           ref={fileInput => this.fileInput = fileInput}/>
                    <p className='ngo-pro-accept'>Do you accept the terms and conditions of our platform?</p>
                    <div>
                    <input className='ngo-terms-radio' value='Yes' type="radio" id="non-profit-terms" name="radioB" onClick={this.handleTermsInput} />
                        <label className='ngo-terms-radio-label' htmlFor="non-profit-terms">yes</label>
                    <input className='project-terms-radio' value='No' type="radio" id="project-terms" name="radioB" onClick={this.handleTermsInput} />
                        <label className='project-terms-radio-label' htmlFor="project-terms">no</label>
                    </div>
                        <button className='btn-org-create-profile' onClick={()=>this.handleCreateProfile()}>Create profile</button>
                </div>
                <div className='desc-popup-screen'>
                    {this.state.showDescriptionPopup ?
                    <DescriptionPopup
                            closePopup={this.toggleDescriptionPopup.bind(this)}
                            value_Description={this.state.information.organisationDescription}
                            onChange_Description={this.changeDescriptionValue} />  : null  }
                </div>
                <div className='focus-popup-screen'>
                    {this.state.showFocusPopup ?
                    <FocusPopup
                            closePopup={this.toggleFocusPopup.bind(this)}
                            value_Social={this.state.information.organisationFocus.social}
                            onChange_Social={this.changeSocialValue}
                            value_Languages={this.state.information.organisationFocus.languages}
                            onChange_Languages={this.changeLanguagesValue}
                            value_Sports={this.state.information.organisationFocus.sports}
                            onChange_Sports={this.changeSportsValue}
                            value_ArtsCulture={this.state.information.organisationFocus.artsCulture}
                            onChange_ArtsCulture={this.changeArtsCultureValue}
                            value_Coaching={this.state.information.organisationFocus.coaching}
                            onChange_Coaching={this.changeCoachingValue}
                            value_Food={this.state.information.organisationFocus.food}
                            onChange_Food={this.changeFoodValue}
                            value_Politics={this.state.information.organisationFocus.politics}
                            onChange_Politics={this.changePoliticsValue}
                            value_Items={this.state.information.organisationFocus.items}
                            onChange_Items={this.changeItemsValue} />  : null  }
                </div>
                <div className='contact-popup-screen'>
                    {this.state.showContactPopup ?
                    <ContactPopup
                            closePopup={this.toggleContactPopup.bind(this)}
                            value_Website={this.state.information.organisationContact.website}
                            onChange_Website={this.changeWebsiteValue}
                            value_Phone={this.state.information.organisationContact.phone}
                            onChange_Phone={this.changePhoneValue}
                    />  : null  }
                </div>
                <div className='contact-popup-screen'>
                    {this.state.showProfilePopup ?
                    <ProfilePopup
                            closePopup={this.toggleProfilePopup.bind(this)}
                            value_Facebook={this.state.information.organisationProfile.facebook}
                            onChange_Facebook={this.changeFacebookValue}
                            value_Instagram={this.state.information.organisationProfile.instagram}
                            onChange_Instagram={this.changeInstagramValue}
                            value_LinkedIn={this.state.information.organisationProfile.linkedIn}
                            onChange_LinkedIn={this.changeLinkedInValue}
                    />  : null  }
                </div>
                <div className='contact-privacy-screen'>
                    {this.state.showPrivacyPopup ?
                    <PrivacyPopup
                            closePopup={this.togglePrivacyPopup.bind(this)}
                            handlePrivacyInput={this.handlePrivacyInput}
                    />  : null  }
                </div>
            </div>
        )
    }
}

export default connect()(Feed)


