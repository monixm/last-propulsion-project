import {baseUrl} from "../constants";

export const createNGOProjectProfileAction = (content) => async (dispatch, getState) => {
    console.log(content)
    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    const validation = {
        code: content.code,
        email: content.email,
        password: content.password,
        password_repeat: content.passwordConfirm,
        first_name: content.organisationName,
        last_name: content.organisationName,
    };

    const organisation = {
        name: content.organisationName,
        type: content.organisationType,
        privacy_setting: content.privacy_setting,
        location: content.organisationLocation,
        description: content.organisationDescription,
        document: content.organisationDocument,
        profile_pic: content.profilePicture,
        website: content.organisationContact.website,
        phone: content.organisationContact.phone,
        terms_of_services: content.organisationTerms,
        focus: {
            social: content.organisationFocus.social,
            languages: content.organisationFocus.languages,
            sports: content.organisationFocus.sports,
            arts_culture: content.organisationFocus.artsCulture,
            coaching: content.organisationFocus.coaching,
            food: content.organisationFocus.food,
            politics: content.organisationFocus.politics,
            items: content.organisationFocus.items,
        },
        organisationProfile: {
            facebook: content.facebook,
            instagram: content.instagram,
            linkedIn: content.linkedIn
        }
    };

    const body = JSON.stringify({validation, organisation});

    const config = {
        method: 'POST',
        headers,
        body
    };

    console.log('body', body)

    const response = await fetch(`${baseUrl}backend/api/organisations/new/`, config);
    return await response.json();
};
