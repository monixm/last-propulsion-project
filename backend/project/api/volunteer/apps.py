from django.apps import AppConfig


class VolunteerConfig(AppConfig):
    name = 'project.api.volunteer'
