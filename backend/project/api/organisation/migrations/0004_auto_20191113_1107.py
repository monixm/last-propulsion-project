# Generated by Django 2.2.6 on 2019-11-13 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organisation', '0003_auto_20191113_1015'),
    ]

    operations = [
        migrations.AddField(
            model_name='organisation',
            name='facebook',
            field=models.URLField(blank=True, max_length=100, null=True, verbose_name='facebook'),
        ),
        migrations.AddField(
            model_name='organisation',
            name='instagram',
            field=models.URLField(blank=True, max_length=100, null=True, verbose_name='instagram'),
        ),
        migrations.AddField(
            model_name='organisation',
            name='linkedin',
            field=models.URLField(blank=True, max_length=100, null=True, verbose_name='LinkedIn'),
        ),
        migrations.AlterField(
            model_name='organisation',
            name='terms_of_services',
            field=models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=False),
        ),
    ]
