# Generated by Django 2.2.6 on 2019-11-08 10:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('volunteer', '0001_initial'),
        ('organisation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('picture', models.ImageField(blank=True, null=True, upload_to='media-files/event/images')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('start_datetime', models.DateTimeField()),
                ('end_datetime', models.DateTimeField()),
                ('location', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True, max_length=500)),
                ('must_be_approved', models.BooleanField(default=False)),
                ('organisation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='event', to='organisation.Organisation')),
                ('participants', models.ManyToManyField(related_name='event', to='volunteer.Volunteer')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
